import React from 'react'



function SideContainerComp() {
    return (
        <div>

            <section className="side-container">
                <ul>
                    <li><img src="/images/menu.png" /></li>
                    <li><img src="/images/gmail.png" /></li>
                    <li className="active"><img src="/images/skype.png" /></li>
                    <li><img src="/images/fbook.png" /></li>
                    <li><img src="/images/whatsapp.png" /></li>
                    <li><img src="/images/plus3.png" /></li>

                </ul>
            </section>

        </div>
    )
}

export default SideContainerComp
