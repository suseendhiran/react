import React, { Component } from 'react'
import './ContactStyles.scss'



export class MessageContainerClassComp extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        let number = this.props.isClicked ? this.props.imageno : 15
        return (
            <div>
                <section className="message-container">
                    <div className="main-contact">
                        <div>
                            <h2> {this.props.name} <span>is typing...</span></h2>
                        </div>
                        <div className="image-flex">
                            <div>
                                <img src="images/star3.png" />
                            </div>
                            <div>
                                <img src="/images/phone3.png" />
                            </div>
                            <div>
                                <img src="/images/video2.png" />
                            </div>
                        </div>
                    </div>
                    <div className="d-flex message-wrapper">
                        <div className="content-wrapper p-3 w-100">

                            <div className="profile-icon">

                                <img className="rounded-circle " src={require(`../../images/${number}.jpg`)} />
                                <span className="message-time">9:30</span>
                            </div>
                            <div className="main-message-content p-2">
                                <p className="m-0">Hey man, what are you doing, how is your life going? </p>
                            </div>
                        </div>
                        <div className="content-wrapper p-3 w-100">

                            <div className="profile-icon">
                                <img src="/images/man1.png" />
                                <span className="message-time">9:32</span>
                            </div>
                            <div className="main-message-content p-2">
                                <p className="m-0">Heloo thertsds ds </p>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex message-wrapper">
                        <div className="content-wrapper p-3 w-100">

                            <div className="profile-icon">
                                <img className="rounded-circle " src={require(`../../images/${number}.jpg`)} />
                                <span className="message-time">9:34</span>
                            </div>
                            <div className="main-message-content p-2">
                                <p className="m-0">Hey man, what are you doing? </p>
                            </div>
                        </div>
                        <div className="content-wrapper p-3 w-100">

                            <div className="profile-icon">
                                <img src="/images/man1.png" />
                                <span className="message-time">9:36</span>
                            </div>
                            <div className="main-message-content p-2">
                                <div className="image-message">
                                    <div className="spidey-image">
                                        <img src="/images/sp4.jpg" />
                                    </div>

                                    <div className="movie">
                                        <div>
                                            <p><b>Homecoming</b> / Action</p>
                                        </div>
                                        <div className="rating">
                                            <img src="/images/rating.png" />
                                            <img src="/images/rating.png" />
                                            <img src="/images/rating.png" />
                                            <img src="/images/rating.png" />
                                            <img src="/images/rating.png" />
                                        </div>
                                    </div>
                                    <p>Spider-Man: Homecoming is a 2017 American superhero film based on the Marvel Comics character Spider-Man</p>
                                    <button type="button" className="btn btn-secondary " aria-haspopup="true" aria-expanded="false">
                                        <p>View</p>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="message-typebox">
                        <div className="linkbox">
                            <img src="/images/link.png" />
                        </div>
                        <div className="typebox">
                            <input type="text" placeholder="Type your message" />
                        </div>
                        <div className="emoji">
                            <button type="button" className="btn btn-secondary " aria-haspopup="true" aria-expanded="false">
                                <img src="/images/emoji1.png" />
                            </button>

                        </div>
                        <div className="entericon">
                            <button type="button" className="btn btn-secondary " aria-haspopup="true" aria-expanded="false">
                                <img src="/images/enter.png" />
                            </button>


                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default MessageContainerClassComp
