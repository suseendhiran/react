import React, { Component } from 'react'


export class ProfileContaierclasscomp extends Component {
    constructor(props) {
        super(props)


    }


    render() {

        let number = this.props.isClicked ? this.props.imageno : 15


        return (



            < div >


                <section className="profileWidth">
                    <div className="profile-wrapper profileWidth">
                        <div className="notification">
                            <img src="/images/star3.png" />
                        </div>
                        <div>
                            <div className="btn-group btn-small">
                                <button type="button" className="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {this.props.name}
                                </button>

                                <div className="dropdown-menu dropdown-menu-right">
                                    <button className="dropdown-item" type="button">Action</button>
                                    <button className="dropdown-item" type="button">Another action</button>
                                    <button className="dropdown-item" type="button">Something else here</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="mail-line">
                        <div>
                            <img src="/images/gmail.png" />
                        </div>
                        <div>
                            <img src="/images/dots.png" />
                        </div>
                    </div>
                    <div className="person-description">
                        <div>
                            <img src={require(`../../images/${number}.jpg`)} />




                        </div>
                        <div>
                            <p>{this.props.nickname}</p>

                        </div>
                        <div className="location">
                            <p>New york,USA</p>
                        </div>
                    </div>
                    <div className="desc">
                        <div className="name-row-flex">

                            <p>Nickname</p>
                            <p><b>{this.props.nickname}</b></p>



                        </div>
                        <div className="name-row-flex">
                            <p>Tel:</p>
                            <p><b>{this.props.telNumber}</b></p>

                        </div>
                        <div className="name-row-flex">
                            <p>Date of birth:</p>
                            <p><b>{this.props.dob}</b></p>

                        </div>
                        <div className="name-row-flex">
                            <p>Gender:</p>
                            <p><b>{this.props.gender}</b></p>

                        </div>
                        <div className="name-row-flex">
                            <p>Language:</p>
                            <p><b>{this.props.language}</b></p>

                        </div>


                    </div>

                </section>

            </div >
        )
    }
}

export default ProfileContaierclasscomp
