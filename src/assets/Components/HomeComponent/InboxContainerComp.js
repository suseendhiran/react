import React from 'react'

function InboxContainerComp() {
    return (
        <div>
            <section className="inbox-container">
                <ul>
                    <li className="inbox-title"> <h6>Inbox</h6><img src="/images/admin.png" /></li>
                    <li><span>All</span> <span>89</span></li>

                    <li className="list-active"><span>Messages</span> <span>11</span></li>
                    <li><span className="active">Unread</span> <span>4</span></li>


                    <li><span>Important</span> <span>1</span></li>
                    <li><span>Teams</span> <span>2</span></li>
                    <li><span>Groups</span> <span>4</span></li>
                    <li><span>Channels</span> <span>2</span></li>
                    <li><span>Locations</span> <span></span></li>
                    <li><span>Media</span> <span>88</span></li>
                    <li><span>Help</span> <span></span></li>
                    <li><span>Settings</span> <span></span></li>

                </ul>
            </section>

        </div>
    )
}

export default InboxContainerComp
