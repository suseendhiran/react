import React from 'react'
import SideContainerComp from './SideContainerComp';
import InboxContainerComp from './InboxContainerComp';
import ContactContainerComp from './ContactContainerComp';
import '../myStyles.scss'
import ContactContainernew from './ContactContainernew';

function MainHome() {
    return (
        <div>
            <section className="home-section">
                <SideContainerComp />
                <InboxContainerComp />
                {/*<ContactContainerComp />*/}
                <ContactContainernew />
            </section>
        </div>
    )
}

export default MainHome
