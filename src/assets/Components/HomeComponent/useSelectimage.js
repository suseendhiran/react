import { useState } from 'react'

function useSelectimage(initialState = 15, isClicked, imageno) {
    const [imageNo, setImageno] = useState(initialState)

    imageNo = isClicked ? setImageno(imageno) : initialState

    return imageNo
}

export default useSelectimage