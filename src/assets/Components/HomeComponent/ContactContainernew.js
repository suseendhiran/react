import React, { useState, useReducer, useEffect } from 'react'
import './ContactStyles.scss'
import ProfileContaierclasscomp from './ProfileContaierclasscomp'
import MessageContainerClassComp from './MessageContainerClassComp'
import axios from 'axios'




var InitialState = {
    name: '',
    number: '',
    dob: '',
    gender: '',
    language: '',
    imageno: '',
    isClicked: '',
    style: ''
}
const InitialTelNum = ''


const reducer = (state = InitialState, action) => {
    console.log(state);
    console.log(action)
    switch (action) {
        case 'SetTonyname':

            {
                return { ...state, name: 'Tony stark', nickName: 'Ironman', number: '+63-54124587', dob: '02-09-1996', gender: 'Male', language: 'English', imageno: 6, isClicked: 'true' }
            }
        case 'SetPetename':
            {
                return { name: 'Peter Parker', nickName: 'Spiderman', number: '+63-54125454', dob: '02-09-1996', gender: 'Male', language: 'Spanish', imageno: 1, isClicked: 'true' }
            }
        case 'SetBrucename':
            {
                return { name: 'Bruce Banner', nickName: 'Hulk', number: '+63-55454545', dob: '02-12-1996', gender: 'Male', language: 'English', imageno: 2, isClicked: 'true' }
            }
        case 'SetLokiname':
            {
                return { name: 'Loki Odinson', nickName: 'Loki', number: '+63-5545564', dob: '07-12-1998', gender: 'Male', language: 'English', imageno: 3, isClicked: 'true' }
            }
        case 'SetCaptainname':
            {
                return { name: 'Steve Rogers', nickName: 'Captain', number: '+63-5878546', dob: '02-12-1820', gender: 'Male', language: 'English', imageno: 4, isClicked: 'true' }
            }
        case 'SetPanthername':
            {
                return { name: 'Tchala', nickName: 'Black panther', number: '+63-5214587', dob: '02-05-1992', gender: 'Male', language: 'English', imageno: 5, isClicked: 'true' }
            }



        default: {
            return InitialState
        }
    }
}


function ContactContainernew() {
    const [nameone, setnameone] = useState("Tony")
    const [posts, setPosts] = useState([])


    const [Userinfo, dispatch] = useReducer(reducer, InitialState)

    useEffect(() => {
        axios.get('./contactdata.json')
            .then(response => {
                console.log(response.data)
                setPosts(response.data)
            })
            .catch(err => {
                console.log(err)
            })
    }, [])




    return (
        <div className='h-100'>
            {posts.map(post => (<div className='MessageBoxProfileWrapper'>

                <section className="contact-container">
                    <div className="contact-header">
                        <div className="form-group has-search">
                            <span className="fa fa-search form-control-feedback"></span>
                            <input type="text" className="form-control" placeholder="Search"></input>
                        </div>
                    </div>
                    <div className="contact-list">
                        <ul>
                            {
                                posts.map(post => (
                                    <li onClick={() => dispatch(post.dispatch)} id="contact-list-sep" className={Userinfo.imageno == post.imageno ? 'active' : ''} >
                                        <div className="conatct-profile">
                                            <img className="rounded-circle" src={require(`../../images/${post.imageno}.jpg`)} />
                                        </div>
                                        <div className="contact-description">
                                            <h5 key={post.id}>{post.name}</h5>
                                            <p>{post.message} </p>
                                        </div>
                                        <div className="conatct-action">
                                            <i className="fas fa-ellipsis-h"></i>
                                            <p>{post.lstime} Min</p>
                                        </div>
                                    </li>))
                            }
                        </ul>
                    </div>
                </section>


                <MessageContainerClassComp name={Userinfo.name} isClicked={Userinfo.isClicked} imageno={Userinfo.imageno} />

                <ProfileContaierclasscomp name={Userinfo.name} nickname={Userinfo.nickName} telNumber={Userinfo.number} dob={Userinfo.dob} gender={Userinfo.gender} language={Userinfo.language} imageno={Userinfo.imageno} isClicked={Userinfo.isClicked} />



            </div>))}
        </div>

    )
}

export default ContactContainernew
