import React, { useState, useReducer } from 'react'
import './ContactStyles.scss'
import ProfileContaierclasscomp from './ProfileContaierclasscomp'
import MessageContainerClassComp from './MessageContainerClassComp'



var InitialState = {
    name: '',
    number: '',
    dob: '',
    gender: '',
    language: '',
    imageno: '',
    isClicked: '',
    style: ''
}
const InitialTelNum = ''


const reducer = (state = InitialState, action) => {
    console.log(state);
    console.log(action)
    switch (action) {
        case 'SetTonyname':

            {
                return { ...state, name: 'Tony stark', nickName: 'Ironman', number: '+63-54124587', dob: '02-09-1996', gender: 'Male', language: 'English', imageno: 6, isClicked: 'true' }
            }
        case 'SetPetename':
            {
                return { name: 'Peter Parker', nickName: 'Spiderman', number: '+63-54125454', dob: '02-09-1996', gender: 'Male', language: 'Spanish', imageno: 1, isClicked: 'true' }
            }
        case 'SetBrucename':
            {
                return { name: 'Bruce Banner', nickName: 'Hulk', number: '+63-55454545', dob: '02-12-1996', gender: 'Male', language: 'English', imageno: 2, isClicked: 'true' }
            }
        case 'SetLokiname':
            {
                return { name: 'Loki Odinson', nickName: 'Loki', number: '+63-5545564', dob: '07-12-1998', gender: 'Male', language: 'English', imageno: 3, isClicked: 'true' }
            }
        case 'SetCaptainname':
            {
                return { name: 'Steve Rogers', nickName: 'Captain', number: '+63-5878546', dob: '02-12-1820', gender: 'Male', language: 'English', imageno: 4, isClicked: 'true' }
            }
        case 'SetPanthername':
            {
                return { name: 'Tchala', nickName: 'Black panther', number: '+63-5214587', dob: '02-05-1992', gender: 'Male', language: 'English', imageno: 5, isClicked: 'true' }
            }



        default: {
            return InitialState
        }
    }
}


function ContactContainerComp() {
    const [nameone, setnameone] = useState("Tony")

    const [Userinfo, dispatch] = useReducer(reducer, InitialState)




    return (
        <div className='MessageBoxProfileWrapper'>



            <section className="contact-container">
                <div className="contact-header">
                    <div className="form-group has-search">
                        <span className="fa fa-search form-control-feedback"></span>
                        <input type="text" className="form-control" placeholder="Search"></input>
                    </div>
                </div>
                <div className="contact-list">
                    <ul>
                        <li onClick={() => dispatch('SetTonyname')} id="contact-list-sep" className={Userinfo.imageno == 6 ? 'active' : ''} >
                            <div className="conatct-profile">

                                <img className="rounded-circle" src="/images/6.jpg" />
                            </div>
                            <div className="contact-description">
                                <h5 >Tony</h5>
                                <p>Lorem ipsum dolor </p>
                            </div>
                            <div className="conatct-action">
                                <i className="fas fa-ellipsis-h"></i>
                                <p>1 Min</p>
                            </div>
                        </li>
                        <li onClick={() => dispatch('SetPetename')} id="contact-list-sep" className={Userinfo.imageno == 1 ? 'active' : ''} >
                            <div className="conatct-profile">
                                <img className="rounded-circle " src="/images/1.jpg" />
                            </div>
                            <div className="contact-description">
                                <h5>Peter</h5>
                                <p>Lorem ipsum dolor </p>
                            </div>
                            <div className="conatct-action">
                                <i className="fas fa-ellipsis-h"></i>
                                <p>1 Min</p>
                            </div>
                        </li>
                        <li onClick={() => dispatch('SetBrucename')} id="contact-list-sep" className={Userinfo.imageno == 2 ? 'active' : ''}>
                            <div className="conatct-profile">
                                <img className="rounded-circle" src="/images/2.jpg" />
                            </div>
                            <div className="contact-description">
                                <h5>Bruce</h5>
                                <p>Lorem ipsum dolor </p>
                            </div>
                            <div className="conatct-action">
                                <i className="fas fa-ellipsis-h"></i>
                                <p>1 Min</p>
                            </div>
                        </li>
                        <li onClick={() => dispatch('SetLokiname')} id="contact-list-sep" className={Userinfo.imageno == 3 ? 'active' : ''}>
                            <div className="conatct-profile">
                                <img className="rounded-circle" src="/images/3.jpg" />
                            </div>
                            <div className="contact-description">
                                <h5>Loki</h5>
                                <p>Lorem iipsum dolor </p>
                            </div>
                            <div className="conatct-action">
                                <i className="fas fa-ellipsis-h"></i>
                                <p>1 Min</p>
                            </div>
                        </li>
                        <li onClick={() => dispatch('SetCaptainname')} id="contact-list-sep" className={Userinfo.imageno == 4 ? 'active' : ''}>
                            <div className="conatct-profile">
                                <img className="rounded-circle" src="/images/4.jpg" />
                            </div>
                            <div className="contact-description">
                                <h5>Steve</h5>
                                <p>Lorem ipsum dolor </p>
                            </div>
                            <div className="conatct-action">
                                <i className="fas fa-ellipsis-h"></i>
                                <p>1 Min</p>
                            </div>
                        </li>
                        <li onClick={() => dispatch('SetPanthername')} id="contact-list-sep" className={Userinfo.imageno == 5 ? 'active' : ''}>
                            <div className="conatct-profile">
                                <img className="rounded-circle" src="/images/5.jpg" />
                            </div>
                            <div className="contact-description">
                                <h5>Tchala</h5>
                                <p>Lorem ipsum dolor </p>
                            </div>
                            <div className="conatct-action">
                                <i className="fas fa-ellipsis-h"></i>
                                <p>1 Min</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            {console.log('hjhjhj', InitialState)}
            {/*<MessageContainerComp />*/}
            <MessageContainerClassComp name={Userinfo.name} isClicked={Userinfo.isClicked} imageno={Userinfo.imageno} />

            <ProfileContaierclasscomp name={Userinfo.name} nickname={Userinfo.nickName} telNumber={Userinfo.number} dob={Userinfo.dob} gender={Userinfo.gender} language={Userinfo.language} imageno={Userinfo.imageno} isClicked={Userinfo.isClicked} />
            {/*<ProfileContainerComp name={InitialState.name} telNumber={InitialState.number} dob={InitialState.dob} gender={InitialState.gender} language={InitialState.language} />*/}


        </div>
    )
}

export default ContactContainerComp
