import React, { useState } from 'react'
import useForm from './useForm'
import validate from './validateLogin'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import MainHome from '../HomeComponent/MainHome';
import { useFormik } from 'formik'


function LoginfunCompnew() {

    const [correct, setCorrect] = useState(false)

    const formik = useFormik({
        initialValues: {
            userName: '',
            password: '',
            noerror: false
        },

        onSubmit: (values) => {
            console.log('Form data : ', values)
            setCorrect(true)
        },
        validate: (values) => {
            let errors = {};

            if (!values.userName) {
                errors.userName = 'Username Required'
            }
            else if (values.userName == 'admin') {
                errors.userName = 'Nice try!!'
            }
            else if (!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/i.test(values.userName)) {
                errors.userName = 'Invalid Username'
            }
            if (!values.password) {
                errors.password = 'Password Required'
            }
            else if (!/(?=^.{8,15}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/i.test(values.password)) {
                errors.password = 'Invalid Password'
            }


            return errors
        },

    })

    console.log(formik.values)



    return (
        <div>
            <div id="background"></div>

            <div className=" container h-100">
                <div className="row h-100 justify-content-center align-items-center">
                    <div className="col">

                    </div>
                    <div className="col-sm-12 col-md-8 col-lg-4">
                        <div className="card  " style={{ width: "333px" }} >
                            <div className="login-card">
                                <img className="card-img-top rounded-circle   profile" src="https://www.templatebeats.com/files/images/profile_user.jpg" alt="Card image" />
                                <h4 className="text-center">Login</h4>
                                <p className="text-center">Sign in to your account</p>
                                <div className="card-body">
                                    <form onSubmit={formik.handleSubmit} className=" " noValidate>
                                        <div className="form-group">
                                            <input type="text" className="form-control" {...formik.getFieldProps('userName')} placeholder="your emailll" name="userName" />
                                            {formik.touched.userName && formik.errors.userName ? <p className="error">{formik.errors.userName}</p> : null}
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control " {...formik.getFieldProps('password')} placeholder="password" name="password" />
                                            {formik.touched.password && formik.errors.password ? <p className="error">{formik.errors.password}</p> : null}
                                        </div>
                                        <div>
                                            <span style={{ width: "48%", textAlign: "left", display: "inline-block" }}>
                                                <label>
                                                    <input type="checkbox" value="remember-me" /><span>Remember me</span>


                                                </label>
                                            </span>
                                            <span style={{ width: "50%", textAlign: "right", display: "inline-block" }}>
                                                <button type="submit" className="btn btn-success btn-block" value="Sign In" >Sign in</ button></span>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            <div id="formFooter">
                                <div className="row">
                                    <div className="col-md-6 col-sm-12 ">
                                        <a className="underlineHover" href="#">Connect with</a>

                                    </div>
                                    <div className="col-md-2">
                                        <i className="fab fa-twitter"></i>

                                    </div>
                                    <div className="col-md-2">
                                        <i className="fab fa-facebook"></i>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div className="col">

                    </div>
                </div>
            </div>
            {/* {noError && <Redirect to="/home" />} */}
            {correct ? <Redirect to="/home" /> : null}




        </div>
    )
}

export default LoginfunCompnew
